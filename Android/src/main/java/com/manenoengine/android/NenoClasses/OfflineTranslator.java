package com.manenoengine.android.NenoClasses;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by danie on 2/12/2016.
 */
public class OfflineTranslator {
    private ArrayList<String> ManenoList;
    private int transType = 0;
    private String eng_swa_File = "/res/raw/swa_eng.txt";
    private String swa_eng_File = "/res/raw/eng_swa.txt";
    private Map<String, ArrayList<String>> EngToSwaMap;
    private Map<String, ArrayList<String>> SwaToEngMap;
    int smallestword = 3;
    public OfflineTranslator() {
        ManenoList= new ArrayList();
        EngToSwaMap = new HashMap<String, ArrayList<String>>();
        SwaToEngMap = new HashMap<String, ArrayList<String>>();
        load(eng_swa_File,EngToSwaMap);
        load(swa_eng_File,SwaToEngMap);

    }
    private void load(String file, Map<String, ArrayList<String>> LangMap){
        String strline = null;
        InputStream is = null;
        is = getClass().getResourceAsStream(file);
        if(is != null) {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader bf = new BufferedReader(isr);
            try {
                while((strline = bf.readLine())!=null){
                    String recordtoCheck = strline.toLowerCase();
                    StringTokenizer st = new StringTokenizer(recordtoCheck,"\t");
                    String word = st.nextToken();
                    ArrayList<String> translations = new ArrayList<String>();
                    while(st.hasMoreElements()) //Check if st has more elements (should have only 2, else: error, )
                        translations.add(st.nextToken());
                    //Cleaning up
                    if(word.startsWith("-"))
                        word = word.substring(1);
                    //Checking if word already in library
                    if(LangMap.containsKey(word))
                        LangMap.get(word).addAll(translations);
                    else
                        LangMap.put(word,translations);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public boolean setTransType(int type){
        if((type > 0) && (type <= 4 )) {
            transType = type;
            return true;
        }
        else
            return false;
    }
    public ArrayList<String> translate(String word){
        switch (transType){
            case 1:
                return findWord(word,EngToSwaMap);
            case 2:
                return findWord(word,SwaToEngMap);
            default:
                return null;
        }

    }

    private ArrayList<String> findWord(String word, Map<String,ArrayList<String>> LangMap){
        //This method will allow dev to completely modify how results are returned.
        //It will be here where search optimisation and refining will be done. Feel free to improve
        ManenoList.clear();
        if((word.length() > smallestword))
        {
            for (String eachword : LangMap.keySet()) {
                if ((eachword.contains(word))) {
                    String check = eachword.replaceAll(word,"");
                    if(!(check.length() > 3)) {
                        for (String translations : LangMap.get(eachword)) {
                            ManenoList.add(eachword + " - " + translations);
                        }
                    }
                }
            }
        }
        Collections.sort(ManenoList, new Comparator<String>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(String lhs, String rhs) {
                return Integer.compare(lhs.length(),rhs.length());
            }
        });
        return ManenoList;

    }
}

