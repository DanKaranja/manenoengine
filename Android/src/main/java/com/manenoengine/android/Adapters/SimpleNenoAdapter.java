package com.manenoengine.android.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.manenoengine.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danie on 3/5/2016.
 */
public class SimpleNenoAdapter extends ArrayAdapter {

    Context mContext;
    int layoutResourceId;
    ArrayList<String> data = null;
    public SimpleNenoAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.mContext = context;
        this.data = (ArrayList<String>) objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId,parent,false);
        }

        String neno = data.get(position);
        TextView txt_neno = (TextView) convertView.findViewById(R.id.txtNeno_retListAdapter);
        txt_neno.setText(neno);
        return convertView;

    }
}
