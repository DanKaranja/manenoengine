package com.manenoengine.android.NenoFragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.manenoengine.android.Adapters.SimpleNenoAdapter;
import com.manenoengine.android.LandingActivity;
import com.manenoengine.android.NenoClasses.OfflineTranslator;
import com.manenoengine.android.R;

import java.util.ArrayList;

/**
 * Created by danie on 2/12/2016.
 */
public class Fragment_trans_offline extends LandingActivity.PlaceholderFragment {
    public Fragment_trans_offline() {
        super();
    }
    private Spinner option_spinner;
    private EditText word_edt;
    private ImageButton search_btn;
    private static OfflineTranslator Translator = new OfflineTranslator();
    private SimpleNenoAdapter Adapter;
    private ListView NenoList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_trans_offline, container, false);
        word_edt = (EditText) rootView.findViewById(R.id.edt_engNeno);
        search_btn = (ImageButton) rootView.findViewById((R.id.search_btn));
        option_spinner = (Spinner) rootView.findViewById(R.id.spinner_transType);
        NenoList = (ListView) rootView.findViewById(R.id.nenoList_listview);

        ArrayAdapter<CharSequence> adapter_spinner = ArrayAdapter.createFromResource(rootView.getContext(), R.array.Trans_options, R.layout.offspinner_title);
        adapter_spinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        option_spinner.setAdapter(adapter_spinner);
        option_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Translator.setTransType(position+1);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Send word to translator
                String word = word_edt.getText().toString();
                ArrayList<String> NenoResultList = Translator.translate(word);
                NenoList.setAdapter(new SimpleNenoAdapter(rootView.getContext(),R.layout.fragment_trans_offline_retnenolist_adapter,NenoResultList));
                //Check results
                if(NenoResultList.isEmpty())
                    Toast.makeText(rootView.getContext(),"Nothing",Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(rootView.getContext(),"Found Something",Toast.LENGTH_LONG).show();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
